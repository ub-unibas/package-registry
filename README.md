## Package Registry

This repository hosts the package registry for libraries developed for UB projects. 
To include them in a project the project has to be added as a repository, 
registry or index to the project.

The packages can be found [here](https://gitlab.com/ub-unibas/package-registry/-/packages).

### Install Package on Server
To install any of the packages `pip` has to be aware of this repository. No token is required, as the registry is public.

```sh
pip install <package-name> --extra-index-url https://gitlab.com/api/v4/projects/29120915/packages/pypi/simple
```

Alternatively the following line can be added to the `requirements.txt` file at the top of the file.

```txt
--extra-index-url https://gitlab.com/api/v4/projects/29120915/packages/pypi/simple
```

Currently I have not found a way to install these packages with setup.py.


### Add Package with CI/CD
In order to add a new package to the registry the repository needs to be setup properly.

1. add a copy of the following .gitlab-ci.yml file to the repository: 
    https://gitlab.com/ub-unibas/einzelprojekte/ub-unibas-utilities/-/blob/master/.gitlab-ci.yml
2. Adjust the package name to match the repository name.
3. Add the $PYPIRC variable to the repository under Settings > CI/CD > Variables. The value should look like below:

```
[distutils]
index-servers = gitlab

[gitlab]
repository = https://gitlab.com/api/v4/projects/29120915/packages/pypi
username = __token__
password = <insert-token>
```
4. Replace the token value with a deploy token generated from this repository or use the same value as used in the project above. ([See here under Variables](https://gitlab.com/ub-unibas/einzelprojekte/ub-unibas-utilities/-/settings/ci_cd)).


